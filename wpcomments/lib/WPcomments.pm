package WPcomments;
use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift;

  # Load configuration from hash returned by "my_app.conf"
  my $config = $self->plugin('Config');

  # Router
  my $r = $self->routes;

  # Normal route to controller
  
  $r->get('/')->to('Wordpress#index');
  $r->get('/index')->to('Wordpress#index');
  $r->get('/check')->to('Wordpress#check');

}

1;
