package WPcomments::Controller::Wordpress;
use Mojo::Base 'Mojolicious::Controller';
use Mojo::URL;
use Mojo::Util 'dumper';

sub check {
    my $self = shift;

    my $v = $self->validation;
    $v->required('url');
    return $self->render(action => 'index', result => undef) if $v->has_error;

    my $url = Mojo::URL->new($v->output->{url});
    my $path = $url->path->trailing_slash(1)->merge('feed');
    $url = $url->path($path)->to_string;
    
    my $ua  = $self->app->ua;
    $ua->max_redirects(1);

    my $res = $ua->get($url)->result->dom->find("item");

    return $self->render(template => 'wordpress/index', result => $res);
}

sub index {
    my $self = shift; 
    return $self->render(result => undef);
}

1;
